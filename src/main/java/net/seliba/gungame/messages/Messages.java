package net.seliba.gungame.messages;

import java.util.HashMap;
import java.util.Map;
import net.seliba.gungame.configuration.Config;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class Messages {

  private static Config messagesConfig;
  private static Map<MessageType, String> loadedMessages = new HashMap<>();

  public Messages(JavaPlugin javaPlugin) {

    //Define the Config of the messages
    Messages.messagesConfig = new Config("messages.yml", javaPlugin);

    //Set default messages
    this.setDefaultMessages();

    //Reload all messages
    Messages.reload();

  }

  private void setDefaultMessages() {

    //Save the default messages to the Config if it doesn't exist
    Messages.messagesConfig.setDefault(MessageType.PREFIX.getIdentifier(), "&6DeinServer &7»");
    Messages.messagesConfig.setDefault(MessageType.NO_PLAYER.getIdentifier(), "&cDazu musst du ein Spieler sein!");
    Messages.messagesConfig.setDefault(MessageType.JOIN.getIdentifier(), "&7[&a+&7] %PLAYER%");
    Messages.messagesConfig.setDefault(MessageType.QUIT.getIdentifier(), "&7[&c-&7] %PLAYER%");
    Messages.messagesConfig.setDefault(MessageType.SPAWN_DAMAGE.getIdentifier(), "&cDu darfst im Spawn-Bereich keine Spieler angreifen!");
    Messages.messagesConfig.setDefault(MessageType.DEATH_SELF.getIdentifier(), "&7Du bist gestorben");
    Messages.messagesConfig.setDefault(MessageType.KILL.getIdentifier(), "&aDu hast &6%PLAYER% &agetötet!");
    Messages.messagesConfig.setDefault(MessageType.GOT_KILLED.getIdentifier(), "&aDu wurdest von &6%PLAYER% &agetötet!");
    Messages.messagesConfig.setDefault(MessageType.LEVEL_UP.getIdentifier(), "&aDu bist nun in &6Level %LEVEL%&a!");
    Messages.messagesConfig.setDefault(MessageType.NEXT_VOTING.getIdentifier(), "&aDas nächste Map-Voting startet in &6%TIME%&a!");
    Messages.messagesConfig.setDefault(MessageType.MAP_CHANGED.getIdentifier(), "&aDie Map wurde zu &6%MAP% &ageändert!");
    Messages.messagesConfig.setDefault(MessageType.VOTING_LORE.getIdentifier(), "&aStimme mit einem Klick für &6%NAME% &aab!");
    Messages.messagesConfig.setDefault(MessageType.VOTING_STARTED.getIdentifier(), "&aDas Map-Voting hat begonnen!");
    Messages.messagesConfig.setDefault(MessageType.SETUP_NAME.getIdentifier(), "&aBitte gebe den Namen der Map in den Chat ein!");
    Messages.messagesConfig.setDefault(MessageType.SETUP_MATERIAL.getIdentifier(), "&aBitte halte das Item der Map in der Hand und benutze den Command &6/gungame setup&a!");
    Messages.messagesConfig.setDefault(MessageType.SETUP_SPAWN_LOCATION.getIdentifier(), "&aBitte stelle dich an den Spawn und benutze den Command &6/gungame setup&a!");
    Messages.messagesConfig.setDefault(MessageType.SETUP_BORDER_LOCATION_1.getIdentifier(), "&aBitte stelle dich an ein Ende des Spawn-Bereiches und benutze den Command &6/gungame setup&a!");
    Messages.messagesConfig.setDefault(MessageType.SETUP_BORDER_LOCATION_2.getIdentifier(), "&aBitte stelle dich an das gegenüberliegende Ende des Spawn-Bereiches und benutze den Command &6/gungame setup&a!");
    Messages.messagesConfig.setDefault(MessageType.SETUP_MIN_Y.getIdentifier(), "&aBitte stelle dich an den untersten Punkt der Map und benutze den Command &6/gungame setup&a!");
    Messages.messagesConfig.setDefault(MessageType.SETUP_FINISH_CONFIRMATION.getIdentifier(), "&aDie Einrichtung ist fertig, bitte bestätige die Map-Erstellung mit dem Command &6/gungame setup&a!");
    Messages.messagesConfig.setDefault(MessageType.SETUP_FINSHED.getIdentifier(), "&aDie Map wurde erfolgreich erstellt!");
    Messages.messagesConfig.save();

  }

  public static String get(MessageType messageType) {

    //Load the message from the intern saved Map
    String message = loadedMessages.get(messageType);

    //Return "null" if the message is not saved
    if(message == null) {
      return "null";
    }

    //Translate the color codes and return it
    return ChatColor.translateAlternateColorCodes(
        '&',
        message
    );

  }

  public static void reload() {

    //Clear the intern saved messages
    Messages.loadedMessages.clear();

    //Load all needed messages from the Config
    for(int i = 0; i < MessageType.values().length; i++) {
      Messages.loadedMessages.put(MessageType.values()[i], Messages.messagesConfig.getString(MessageType.values()[i].getIdentifier()) + " ");
    }

  }

}
