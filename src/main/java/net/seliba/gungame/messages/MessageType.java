package net.seliba.gungame.messages;

public enum MessageType {

  PREFIX("prefix"),
  JOIN("join"),
  QUIT("quit"),
  SPAWN_DAMAGE("spawn_damage"),
  DEATH_SELF("death_self"),
  KILL("kill"),
  GOT_KILLED("got_killed"),
  LEVEL_UP("levelup"),
  NEXT_VOTING("next_voting_announcement"),
  MAP_CHANGED("map_change"),
  VOTING_LORE("voting_lore"),
  VOTING_STARTED("voting_started"),
  NO_PLAYER("no_player"),
  NO_ITEM("no_item"),
  SETUP_NAME("setup_name"),
  SETUP_MATERIAL("setup_material"),
  SETUP_SPAWN_LOCATION("setup_spawn_location"),
  SETUP_BORDER_LOCATION_1("setup_border_location_1"),
  SETUP_BORDER_LOCATION_2("setup_border_location_2"),
  SETUP_MIN_Y("setup_miny"),
  SETUP_FINISH_CONFIRMATION("setup_finish_confirmation"),
  SETUP_FINSHED("setup_finished");

  private String identifier;

  MessageType(String identifier) {

    this.identifier = identifier;

  }

  public String getIdentifier() {

    return identifier;

  }

}
