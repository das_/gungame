package net.seliba.gungame.utils;

import java.util.Arrays;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

/**
 * A class which allows to build ItemStacks from scratch.
 */
public class ItemStackBuilder {

  private ItemStack itemStack;
  private ItemMeta itemMeta;

  /**
   * Constructor which allows to build ItemStacks
   * with a certain Material.
   * @param material The Material of the ItemStack.
   */
  public ItemStackBuilder(Material material) {

    this.itemStack = new ItemStack(material);
    this.itemMeta = itemStack.getItemMeta();

  }

  /**
   * Constructor which allows to modify an existing
   * ItemStack.
   * @param itemStack The ItemStack.
   */
  public ItemStackBuilder(ItemStack itemStack) {

    this.itemStack = itemStack;
    this.itemMeta = itemStack.getItemMeta();

  }

  /**
   * Changes the name of the ItemStack.
   * @param name The new name of the ItemStack.
   * @return An instance of the current ItemStackBuilder.
   */
  public ItemStackBuilder name(String name) {

    this.itemMeta.setDisplayName(name);
    return this;

  }

  public ItemStackBuilder lore(String lore) {

    this.itemMeta.getLore().addAll(Arrays.asList(lore.split("\n")));
    return this;

  }

  /**
   * Changes the amount of the ItemStack.
   * @param amount The new amount of the ItemStack.
   * @return An instance of the current ItemStackBuilder.
   */
  public ItemStackBuilder amount(int amount) {

    this.itemStack.setAmount(amount);
    return this;

  }

  /**
   * Adds an unsafe enchantment to the ItemStack.
   * @param enchantment The Enchantment which should be assigned.
   * @param level The level of the Enchantment.
   * @return An instance of the current ItemStackBuilder.
   */
  public ItemStackBuilder enchantment(Enchantment enchantment, int level) {

    this.itemStack.addUnsafeEnchantment(enchantment, level);
    return this;

  }

  /**
   * Changes the color of leather armor.
   * @param color The new color of the ItemStack.
   * @return An instance of the current ItemStackBuilder.
   */
  public ItemStackBuilder color(Color color) {

    if(this.itemStack.getType().name().contains("LEATHER_")) {
      LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) this.itemMeta;
      leatherArmorMeta.setColor(color);
    }
    return this;

  }

  /**
   * Sets the owner of player heads.
   * @param uuid The UUID of the owner.
   * @return An instance of the current ItemStackBuilder.
   */
  public ItemStackBuilder owner(UUID uuid) {

    if(this.itemStack.getType() == Material.SKULL_ITEM) {
      SkullMeta skullMeta = (SkullMeta) this.itemMeta;
      skullMeta.setOwner(Bukkit.getOfflinePlayer(uuid).getName());
    }
    return this;

  }

  /**
   * Builds everything together and returns the final
   * ItemStack created by this class.
   * @return The ItemStack created by this class.
   */
  public ItemStack build() {

    this.itemStack.setItemMeta(itemMeta);
    return this.itemStack;

  }

}
