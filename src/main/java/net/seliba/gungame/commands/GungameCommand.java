package net.seliba.gungame.commands;

import net.seliba.gungame.maps.MapCreator;
import net.seliba.gungame.maps.MapManager;
import net.seliba.gungame.messages.MessageType;
import net.seliba.gungame.messages.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GungameCommand implements CommandExecutor {

  private MapManager mapManager;

  public GungameCommand(MapManager mapManager) {

    this.mapManager = mapManager;

  }

  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

    if(args.length != 1) {
      sendHelpMessage(sender);
      return false;
    }

    switch (args[0]) {
      case "reload":
        Messages.reload();
        break;
      case "setup":
        //Check if the command executor is a Player
        if(!(sender instanceof Player)) {
          sender.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.NO_PLAYER));
          return false;
        }

        //Define the Player
        Player player = (Player) sender;

        if(!player.hasPermission("gungame.setup")) {
          sendHelpMessage(player);
          return false;
        }

        //Define the player
        MapCreator.get(player).nextStep();
        break;
      case "listmaps":
        sender.sendMessage(Messages.get(MessageType.PREFIX) + "§aMaps:");
        if(this.mapManager.getLoadedMaps().isEmpty()) {
          sender.sendMessage("§cKeine");
          return false;
        }
        this.mapManager.getLoadedMaps().forEach(map -> sender.sendMessage("§a- §6" + map.getMapName()));
        break;
      case "startvoting":
        this.mapManager.setVotingRunning(true);
        this.mapManager.setNextVotingTime(System.currentTimeMillis());
        break;
      default:
        sendHelpMessage(sender);
        break;
    }

    return true;

  }

  private void sendHelpMessage(CommandSender sender) {

    sender.sendMessage("§7------------§8[§aHilfe§8]§7------------");
    sender.sendMessage("§6/gungame reload §8| §6Lädt alle Nachrichten neu");
    sender.sendMessage("§6/gungame setup §8| §6Startet das Map-Setup");
    sender.sendMessage("§7------------§8[§aHilfe§8]§7------------");

  }

}
