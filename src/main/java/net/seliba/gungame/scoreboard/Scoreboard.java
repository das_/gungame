package net.seliba.gungame.scoreboard;

import java.util.List;
import net.seliba.gungame.maps.MapManager;
import net.seliba.gungame.stats.Stats;
import net.seliba.gungame.stats.StatsType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

public class Scoreboard {

  private MapManager mapManager;
  private Stats stats;
  private String serverName;
  private List<String> scoreboardList;

  public Scoreboard(MapManager mapManager, Stats stats, String serverName, List<String> scoreboardList) {

    this.mapManager = mapManager;
    this.stats = stats;
    this.serverName = serverName;
    this.scoreboardList = scoreboardList;

  }

  public void updateScoreboard() {

    // Iterate over all Players
    Bukkit.getOnlinePlayers()
        .forEach(
            player -> {
              // Define Scoreboard and objective
              org.bukkit.scoreboard.Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
              Objective objective = scoreboard.registerNewObjective("abc", "§f§l" + serverName);

              // Edit display slot and display name
              objective.setDisplaySlot(DisplaySlot.SIDEBAR);
              objective.setDisplayName("§f§l" + serverName);

              //Change objectives to the value in the Config
              for (int i = scoreboardList.size() - 1; i > 0; i--) {
                objective.getScore(translateString(scoreboardList.get(i), player)).setScore(i);
              }

              // Show Scoreboard to Player
              player.setScoreboard(scoreboard);
            });
  }

  private String translateString(String string, Player player) {

    //Translate everything
    return ChatColor.translateAlternateColorCodes('&', string
        .replaceAll("%KILLS%", String.valueOf(this.stats.get(player, StatsType.KILLS)))
        .replaceAll("%MAX_KILLS%", String.valueOf(this.stats.get(player, StatsType.MAX_KILLS)))
        .replaceAll("%MAP%", this.mapManager.getCurrentMap().getMapName())
        .replaceAll("%TIME%", this.getTimeDifferenceAsString())
    );

  }

  private String getTimeDifferenceAsString() {

    long currentTime = System.currentTimeMillis();
    long difference = this.mapManager.getNextVotingTime() - currentTime;
    long seconds = difference / 1000;
    long minutes = 0;
    while(seconds - 60 >= 0) {
      minutes++;
      seconds = seconds - 60;
    }
    return minutes + ":" + seconds;

  }

}
