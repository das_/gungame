package net.seliba.gungame.listener;

import net.seliba.gungame.maps.MapManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public class EntitySpawnListener implements Listener {

  private MapManager mapManager;

  public EntitySpawnListener(MapManager mapManager) {

    this.mapManager = mapManager;

  }

  @EventHandler
  public void onEntitySpawn(EntitySpawnEvent event) {

    //Check if the map where the weather changed equals the map of the current game
    if(!this.mapManager.equalsCurrentMap(event.getLocation().getWorld())) {
      return;
    }

    //Cancel the event
    event.setCancelled(true);

  }

}
