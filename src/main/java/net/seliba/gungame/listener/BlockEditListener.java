package net.seliba.gungame.listener;

import net.seliba.gungame.maps.MapManager;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockEditListener implements Listener {

  private MapManager mapManager;

  public BlockEditListener(MapManager mapManager) {

    this.mapManager = mapManager;

  }

  @EventHandler
  public void onBlockBreak(BlockBreakEvent event) {

    //Check if the map where the weather changed equals the map of the current game
    if(!this.mapManager.equalsCurrentMap(event.getBlock().getWorld()) || event.getPlayer().getGameMode() == GameMode.CREATIVE) {
      return;
    }

    //Cancel the event
    event.setCancelled(true);

  }

  @EventHandler
  public void onBlockPlace(BlockPlaceEvent event) {

    //Check if the map where the weather changed equals the map of the current game
    if(!this.mapManager.equalsCurrentMap(event.getBlock().getWorld()) || event.getPlayer().getGameMode() == GameMode.CREATIVE) {
      return;
    }

    //Cancel the event
    event.setCancelled(true);

  }

}
