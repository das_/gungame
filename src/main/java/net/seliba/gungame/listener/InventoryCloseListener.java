package net.seliba.gungame.listener;

import net.seliba.gungame.maps.MapManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class InventoryCloseListener implements Listener {

  private MapManager mapManager;

  public InventoryCloseListener(MapManager mapManager) {

    this.mapManager = mapManager;

  }

  @EventHandler
  public void onInventoryClose(InventoryCloseEvent event) {

    //Define the Player
    Player player = (Player) event.getPlayer();

    //Check if the map where the weather changed equals the map of the current game
    if(!this.mapManager.equalsCurrentMap(player.getWorld())) {
      return;
    }

    //Check if there is currently a voting running
    if(!this.mapManager.isVotingRunning()) {
      return;
    }

    //Open the vote Inventory again
    this.mapManager.getMapVoting().startVoting(player);

  }

}
