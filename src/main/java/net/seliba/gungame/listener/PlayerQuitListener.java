package net.seliba.gungame.listener;

import net.seliba.gungame.levels.Levels;
import net.seliba.gungame.maps.MapManager;
import net.seliba.gungame.messages.MessageType;
import net.seliba.gungame.messages.Messages;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

  private MapManager mapManager;
  private Levels levels;

  public PlayerQuitListener(MapManager mapManager, Levels levels) {

    this.mapManager = mapManager;
    this.levels = levels;

  }

  @EventHandler
  public void onPlayerQuit(PlayerQuitEvent event) {

    //Define the Player who left
    Player player = event.getPlayer();

    //Change the quit message
    event.setQuitMessage(Messages.get(MessageType.QUIT).replaceAll("%PLAYER%", player.getName()));

    //Check if there is currently a map voting
    if(!this.mapManager.isVotingRunning()) {
      return;
    }

    //Check if the Player has voted
    if(!this.mapManager.getMapVoting().hasVoted(player)) {
      return;
    }

    //Remove the vote of the Player
    this.mapManager.getMapVoting().removeVote(player);

    //Reset the level of the Player
    this.levels.setLevel(player, 1L);

  }

}
