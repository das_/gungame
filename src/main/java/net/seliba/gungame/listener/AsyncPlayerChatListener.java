package net.seliba.gungame.listener;

import net.seliba.gungame.maps.MapCreator;
import net.seliba.gungame.maps.SetupState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChatListener implements Listener {

  @EventHandler
  public void onAsyncChat(AsyncPlayerChatEvent event) {

    //Define the Player
    Player player = event.getPlayer();

    //Check if the Player is currently setting up a Map
    if(!MapCreator.isInSetup(player)) {
      return;
    }
    //Access the map creator of the Player
    MapCreator playerMapCreator = MapCreator.get(player);

    //Check the next setup step
    if(playerMapCreator.getNextSetupState() != SetupState.NAME) {
      return;
    }

    //Set the name of the Map
    playerMapCreator.setName(event.getMessage());

    //Cancel the event
    event.setCancelled(true);

  }

}
