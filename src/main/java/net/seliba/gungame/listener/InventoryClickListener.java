package net.seliba.gungame.listener;

import net.seliba.gungame.maps.MapManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryClickListener implements Listener {

  private MapManager mapManager;

  public InventoryClickListener(MapManager mapManager) {

    this.mapManager = mapManager;

  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {

    //Check if the Player clicked an ItemStack in an Inventory
    if(event.getClickedInventory() == null || event.getCurrentItem() == null) {
      return;
    }

    //Define the needed variables
    Player player = (Player) event.getWhoClicked();
    String inventoryName = event.getView().getTitle();
    String clickedItemName = event.getCurrentItem().getItemMeta().getDisplayName();

    if(!inventoryName.equals("§aMap-Voting")) {
      return;
    }

    this.mapManager.getMapVoting().addVote(player, this.mapManager.getMapByName(clickedItemName.replaceAll("§6", "")).orElse(this.mapManager.getCurrentMap()));

  }

}
