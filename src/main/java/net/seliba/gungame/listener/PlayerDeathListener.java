package net.seliba.gungame.listener;

import net.seliba.gungame.event.GungameLeveldownEvent;
import net.seliba.gungame.event.GungameLevelupEvent;
import net.seliba.gungame.levels.Levels;
import net.seliba.gungame.maps.MapManager;
import net.seliba.gungame.messages.MessageType;
import net.seliba.gungame.messages.Messages;
import net.seliba.gungame.stats.Stats;
import net.seliba.gungame.stats.StatsType;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class PlayerDeathListener implements Listener {

  private JavaPlugin javaPlugin;
  private MapManager mapManager;
  private Levels levels;
  private Stats stats;
  private long maxLastDamageTime;
  private boolean healOnKill;

  public PlayerDeathListener(JavaPlugin javaPlugin, MapManager mapManager, Levels levels, Stats stats, long maxLastDamageTimeSeconds, boolean healOnKill) {

    this.javaPlugin = javaPlugin;
    this.mapManager = mapManager;
    this.levels = levels;
    this.stats = stats;
    this.maxLastDamageTime = maxLastDamageTimeSeconds * 1000;
    this.healOnKill = healOnKill;

  }

  @EventHandler
  public void onPlayerDeath(PlayerDeathEvent event) {

    //Define the death Player and his last damager
    Player player = event.getEntity();
    Player lastDamager = player.getKiller();

    //Return if the Player didn't died on the Gungame map
    if(!mapManager.equalsCurrentMap(player.getWorld())) {
      return;
    }

    //Send message if there isn't a last damager or the last damage was too long ago and respawn the Player instantly
    if(lastDamager == null || PlayerDamageListener.getLastDamageTime(player) == null || PlayerDamageListener.getLastDamageTime(player) + maxLastDamageTime > System.currentTimeMillis()) {
      player.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.DEATH_SELF));
      player.spigot().respawn();
      return;
    }

    //Send message to the killer and the death Player
    player.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.GOT_KILLED).replaceAll("%PLAYER%", player.getName()));
    lastDamager.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.KILL).replaceAll("%PLAYER%", player.getName()));

    //Increase the level of the killer and trigger the GungameLevelupEvent
    Bukkit.getPluginManager().callEvent(new GungameLevelupEvent(lastDamager, player, levels.getLevel(lastDamager), levels.getLevel(lastDamager) + 1, levels.getEquipment(levels.getLevel(lastDamager))));
    levels.levelUp(lastDamager);

    //Decrease the level of the killed player, trigger the GungameLeveldownEvent
    long oldLevel = levels.getLevel(player);
    levels.levelDown(player);
    Bukkit.getPluginManager().callEvent(new GungameLeveldownEvent(player, lastDamager, oldLevel, levels.getLevel(player), levels.getEquipment(levels.getLevel(player))));

    //Play level-up sound to the killer
    lastDamager.playSound(lastDamager.getLocation(), Sound.LEVEL_UP, 5f, 5f);

    //Heal the killer if wanted
    if(healOnKill) {
      lastDamager.setHealth(20D);
    }

    //Reset the time of the last damage
    PlayerDamageListener.removeLastDamageTime(player);

    //Let the Player respawn instantly
    player.spigot().respawn();

    //Update stats
    this.stats.add(player, StatsType.DEATHS);
    this.stats.add(lastDamager, StatsType.KILLS);
    if(this.stats.get(lastDamager, StatsType.MAX_KILLS) < this.stats.get(player, StatsType.KILLS)) {
      this.stats.add(player, StatsType.MAX_KILLS);
    }

  }

  @EventHandler
  public void onPlayerRespawn(PlayerRespawnEvent event) {

    //Define the Player who respawned
    Player player = event.getPlayer();

    //Reset player with a short delay to fix various bugs
    Bukkit.getScheduler().runTaskLater(javaPlugin, () -> mapManager.respawn(player), 3L);

  }

}
