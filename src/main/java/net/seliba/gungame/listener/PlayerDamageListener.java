package net.seliba.gungame.listener;

import java.util.HashMap;
import net.seliba.gungame.maps.Map;
import net.seliba.gungame.maps.MapManager;
import net.seliba.gungame.messages.MessageType;
import net.seliba.gungame.messages.Messages;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class PlayerDamageListener implements Listener {

  private MapManager mapManager;
  private static java.util.Map<Player, Long> lastDamageTime = new HashMap<>();

  public PlayerDamageListener(MapManager mapManager) {

    this.mapManager = mapManager;

  }

  @EventHandler
  public void onPlayerDamage(EntityDamageByEntityEvent event) {

    //Return if the entity is not a Player
    if(!(event.getEntity() instanceof Player)) {
      return;
    }

    //Define variables
    Player player = (Player) event.getEntity();
    Location location = player.getLocation();
    Map map = mapManager.getCurrentMap();

    //Choose a new Map if there isn't one
    if(map == null) {
      mapManager.chooseRandomMap();
      map = mapManager.getCurrentMap();
    }

    //Return if the Player is not in the world of the current game
    if(!map.getSpawnLocation().getWorld().getName().equals(location.getWorld().getName())) {
      return;
    }

    //Return if the Player is outside of the spawn
    if(location.getBlockX() > map.getMaxSpawnX() || location.getBlockX() < map.getMinSpawnX()) {
      return;
    }

    //Return if the Player is outside of the spawn
    if(location.getBlockZ() > map.getMaxSpawnZ() || location.getBlockZ() < map.getMinSpawnZ()) {
      return;
    }

    //Cancel the event
    event.setCancelled(true);

    //Send message to the Player who damaged in the spawn area
    player.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.SPAWN_DAMAGE));

    //Save the time of the damage
    lastDamageTime.put(player, System.currentTimeMillis());

  }

  static Long getLastDamageTime(Player player) {

    return PlayerDamageListener.lastDamageTime.get(player);

  }

  static void removeLastDamageTime(Player player) {

    PlayerDamageListener.lastDamageTime.remove(player);

  }

}
