package net.seliba.gungame.listener;

import net.seliba.gungame.maps.MapManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class PlayerDropListener implements Listener {

  private MapManager mapManager;
  private boolean disableItemDrop;

  public PlayerDropListener(MapManager mapManager, boolean disableItemDrop) {

    this.mapManager = mapManager;
    this.disableItemDrop = disableItemDrop;

  }

  @EventHandler
  public void onPlayerDrop(PlayerDropItemEvent event) {

    //Check if item dropping should be enabled
    if(!disableItemDrop) {
      return;
    }

    //Check if the map where the weather changed equals the map of the current game
    if(!this.mapManager.equalsCurrentMap(event.getPlayer().getWorld())) {
      return;
    }

    //Cancel the event
    event.setCancelled(true);

  }

}
