package net.seliba.gungame.listener;

import net.seliba.gungame.maps.MapManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WeatherChangeListener implements Listener {

  private MapManager mapManager;
  private boolean cancelWeatherChange;

  public WeatherChangeListener(MapManager mapManager, boolean cancelWeatherChange) {

    this.mapManager = mapManager;
    this.cancelWeatherChange = cancelWeatherChange;

  }

  @EventHandler
  public void onWeatherChange(WeatherChangeEvent event) {

    //Check if weather changing should be enabled
    if(!this.cancelWeatherChange) {
      return;
    }

    //Check if the map where the weather changed equals the map of the current game
    if(!this.mapManager.getCurrentMap().getSpawnLocation().getWorld().getName().equals(event.getWorld().getName())) {
      return;
    }

    //Cancel the event
    event.setCancelled(true);

  }

}
