package net.seliba.gungame.listener;

import net.seliba.gungame.Gungame;
import net.seliba.gungame.levels.Levels;
import net.seliba.gungame.maps.MapManager;
import net.seliba.gungame.messages.MessageType;
import net.seliba.gungame.messages.Messages;
import net.seliba.gungame.utils.ActionBar;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

  private MapManager mapManager;
  private Levels levels;
  private boolean teamsAllowed;

  public PlayerJoinListener(MapManager mapManager, Levels levels, boolean teamsAllowed) {

    this.mapManager = mapManager;
    this.levels = levels;

  }

  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent event) {

    //Define the Player
    Player player = event.getPlayer();

    //Change the join message
    event.setJoinMessage(Messages.get(MessageType.JOIN).replaceAll("%PLAYER%", player.getName()));

    //Reset level
    this.levels.setLevel(player, 1L);

    //Teleport Player to the spawn location
    player.teleport(mapManager.getCurrentMap().getSpawnLocation());

    //Check if there is currently a map voting running
    if(this.mapManager.isVotingRunning()) {
      Bukkit.getScheduler().runTaskLaterAsynchronously(Gungame.getProvidingPlugin(Gungame.class), () -> mapManager.getMapVoting().startVoting(player), 5L);
    }

    //Send action bar and title to Player
    ActionBar.sendActionBarMessage(player, teamsAllowed ? "§aTeaming ist erlaubt" : "§cTeaming ist verboten", Integer.MAX_VALUE, Gungame.getPlugin(Gungame.class));
    player.sendTitle("§7GunGame", "§6ItzYourGames.de");

  }

}
