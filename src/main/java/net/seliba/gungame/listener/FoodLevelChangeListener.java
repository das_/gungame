package net.seliba.gungame.listener;

import net.seliba.gungame.maps.MapManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodLevelChangeListener implements Listener {

  private MapManager mapManager;
  private boolean disableHunger;

  public FoodLevelChangeListener(MapManager mapManager, boolean disableHunger) {

    this.mapManager = mapManager;
    this.disableHunger = disableHunger;

  }

  @EventHandler
  public void onFoodLevelChangeEvent(FoodLevelChangeEvent event) {

    //Check if food level changing should be enabled
    if(!disableHunger) {
      return;
    }

    //Check if the map where the weather changed equals the map of the current game
    if(!this.mapManager.equalsCurrentMap(event.getEntity().getWorld())) {
      return;
    }

    //Cancel the event
    event.setCancelled(true);

  }

}
