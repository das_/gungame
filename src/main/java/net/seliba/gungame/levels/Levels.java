package net.seliba.gungame.levels;

import java.util.HashMap;
import java.util.Map;
import net.seliba.gungame.configuration.Config;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class Levels {

  private static Levels instance;
  private Config levelConfig;
  private long removeLevelAmount;
  private Map<Long, ItemStack[]> levels = new HashMap<>();
  private Map<Player, Long> playerLevels = new HashMap<>();

  public Levels(JavaPlugin javaPlugin, String levelFileName, long removeLevelAmount) {

    instance = this;
    this.levelConfig = new Config(levelFileName, javaPlugin);
    this.removeLevelAmount = removeLevelAmount;
    this.loadLevels();

  }

  private void loadLevels() {

    //Define current level counter
    long i = 0;
    //Infinite loop (because there should be an infinite amount of levels too)
    while(true) {
      //Increase the current level of the loop
      i++;

      //Load ItemStacks of the level from the Config
      ItemStack handItem = levelConfig.getItemStack("levels." + i + ".hand");
      ItemStack helmetItem = levelConfig.getItemStack("levels." + i + ".helmet");
      ItemStack chestplateItem = levelConfig.getItemStack("levels." + i + ".chestplate");
      ItemStack leggingsItem = levelConfig.getItemStack("levels." + i + ".leggings");
      ItemStack bootsItem = levelConfig.getItemStack("levels." + i + ".boots");

      //Return if the level doesn't exist
      if(handItem == null) {
        return;
      }

      //Save the Inventory contents of the level into an ItemStack[]
      ItemStack[] inventoryContent = new ItemStack[5];
      inventoryContent[0] = handItem;
      inventoryContent[1] = helmetItem;
      inventoryContent[2] = chestplateItem;
      inventoryContent[3] = leggingsItem;
      inventoryContent[4] = bootsItem;

      //Save the created ItemStack[] which contains all information about the Inventory content of the current level
      levels.put(i, inventoryContent);
    }

  }

  public void levelUp(Player player) {

    //Access the Player's current level
    Long currentLevel = playerLevels.get(player);

    //Check if the Player has a level
    if(currentLevel == null) {
      currentLevel = 1L;
    }

    //Define the new current level
    currentLevel++;

    //Set the Player's level
    this.setLevel(player, currentLevel);

  }

  public void levelDown(Player player) {

    //Access the Player's current level
    Long currentLevel = playerLevels.get(player);

    //Check if the Player has a level
    if(currentLevel == null) {
      currentLevel = 1L;
    }

    //Estimate the new level
    Long newLevel = currentLevel - removeLevelAmount >= 1 ? currentLevel - removeLevelAmount : 1;

    //Set the Player's level
    this.setLevel(player, newLevel);

  }

  public void setLevel(Player player, Long level) {

    //Save the new level
    playerLevels.put(player, level);

    //Load new Inventory content
    ItemStack[] inventoryContent = getEquipment(level);

    //Set the Inventory content in the Inventory of the Player
    player.getInventory().clear();
    player.getInventory().setItem(0, inventoryContent[0]);
    player.getInventory().setHelmet(inventoryContent[1]);
    player.getInventory().setChestplate(inventoryContent[2]);
    player.getInventory().setLeggings(inventoryContent[3]);
    player.getInventory().setBoots(inventoryContent[4]);

    // Play sound, update level bar
    player.playSound(player.getLocation(), Sound.LEVEL_UP, 1f, 1f);
    player.setLevel(level.intValue());
    player.setExp(level);

  }

  public long getLevel(Player player) {

    //Access the Player's current level
    Long currentLevel = playerLevels.get(player);

    //Check if the Player has a level
    if(currentLevel == null) {
      currentLevel = 1L;
      this.playerLevels.put(player, currentLevel);
    }

    //Return his level
    return currentLevel;

  }

  /**
   * Returns the Inventory content which a Player in this level should have.
   * This Array consists of:
   * 0 = ItemStack in hand
   * 1 = The helmet
   * 2 = The chestplate
   * 3 = The leggings
   * 4 = The boots
   * @param level The level which
   * @return The ItemStacks of the level
   */
  public ItemStack[] getEquipment(long level) {

    //Check if the level is bigger than the max. level defined in the Config
    if(level > levels.size()) {
      //Return the equipment of the max. level
      return levels.get(levels.size() - 1L);
    }

    //Return the equipment of the level
    return levels.get(level);

  }

  public static Levels getInstance() {

    return instance;

  }

}
