package net.seliba.gungame.stats;

public enum StatsType {

  KILLS(),
  DEATHS(),
  MAX_KILLS(),
  KD()

}
