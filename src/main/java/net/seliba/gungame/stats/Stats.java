package net.seliba.gungame.stats;

import net.seliba.gungame.dao.StatsDao;
import org.bukkit.entity.Player;

public class Stats {

  private StatsDao statsDao;

  public Stats(StatsDao statsDao) {

    this.statsDao = statsDao;

  }

  public long get(Player player, StatsType statsType) {

    switch (statsType) {
      case KILLS:
        return statsDao.getKills(player);
      case DEATHS:
        return statsDao.getDeaths(player);
      case MAX_KILLS:
        return statsDao.getMaxKills(player);
      case KD:
        long kills = statsDao.getKills(player);
        long deaths = statsDao.getDeaths(player);

        if(deaths == 0) {
          return kills;
        }

        return Math.round(kills / deaths);
      default:
        //Why tf should this happen?
        return 0;
    }

  }

  public void add(Player player, StatsType statsType) {

    switch (statsType) {
      case DEATHS:
        this.statsDao.addDeath(player);
        break;
      case KILLS:
        this.statsDao.addKill(player);
        break;
      case MAX_KILLS:
        this.statsDao.addMaxKills(player);
        break;
      default:
        //Do nothing
        break;
    }

  }

}
