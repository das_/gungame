package net.seliba.gungame.database;

public class Credentials {

  private String serverName;
  private String databaseName;
  private String username;
  private String password;

  public Credentials(String serverName, String databaseName, String username, String password) {

    this.serverName = serverName;
    this.databaseName = databaseName;
    this.username = username;
    this.password = password;

  }

  String getServerName() {

    return serverName;

  }

  String getDatabaseName() {

    return databaseName;

  }

  String getUsername() {

    return username;

  }

  String getPassword() {

    return password;

  }

}
