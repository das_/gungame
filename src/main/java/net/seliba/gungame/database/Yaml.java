package net.seliba.gungame.database;

import java.util.Optional;
import net.seliba.gungame.Gungame;
import net.seliba.gungame.configuration.Config;
import org.bukkit.Location;

public class Yaml implements Database {

  private Config config;

  @Override
  public void connect(Credentials credentials) {

    this.config = new Config(credentials.getDatabaseName() + ".yml", Gungame.getProvidingPlugin(Gungame.class));

  }

  @Override
  public void disconnect() {

    //For the sake of completeness only
    this.config = null;

  }

  @Override
  public void set(String key, Object value, Condition condition) {

    try {
      if(value instanceof Location) {
        config.setLocation(condition.getValue() + "." + key, (Location) value);
      } else {
        config.set(condition.getValue() + "." + key, value);
      }
    } catch (Exception exception) {
     exception.printStackTrace();
    }

    //Save the Config
    config.save();

  }

  @Override
  public Optional<String> get(String path, Condition condition) {

    try {
      return Optional.ofNullable(config.get(condition.getValue() + "." + path).toString());
    } catch (Exception exception) {
      return Optional.empty();
    }

  }

}
