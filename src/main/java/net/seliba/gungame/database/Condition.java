package net.seliba.gungame.database;

public class Condition {

  private String key;
  private String value;

  public Condition(String conditionKey, String conditionValue) {

    this.key = conditionKey;
    this.value = conditionValue;

  }

  String getKey() {

    return key;

  }

  String getValue() {

    return value;

  }

  @Override
  public String toString() {

    return key + " = " + value;

  }

}
