package net.seliba.gungame.database;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import net.seliba.gungame.Gungame;
import org.bukkit.Bukkit;

public class MySql implements Database {

  private Connection connection;

  @Override
  public void connect(Credentials credentials) {

    MysqlDataSource dataSource = new MysqlDataSource();
    dataSource.setUser(credentials.getUsername());
    dataSource.setPassword(credentials.getPassword());
    dataSource.setServerName(credentials.getServerName());

    try {
      this.connection = dataSource.getConnection();
      this.connection.prepareStatement("CREATE TABLE IF NOT EXISTS stats(uuid VARCHAR(100), kills VARCHAR(100), deaths VARCHAR(100), max_kills VARCHAR(100))").executeUpdate();
    } catch (SQLException exception) {
      Bukkit.getConsoleSender().sendMessage("§cFehler beim Aufbau der MySQL-Verbindung! Beende...");
      Bukkit.getPluginManager().disablePlugin(Gungame.getPlugin(Gungame.class));
    }

  }

  @Override
  public void disconnect() {

    try {
      connection.close();
    } catch (SQLException exception) {
      //Do nothing, connection is probably already closed
    }

  }

  @Override
  public void set(String key, Object value, Condition condition) {

    try {
      PreparedStatement statement = connection.prepareStatement("UPDATE stats SET " + key + " = '" + value + "' WHERE " + condition.toString());
      statement.executeUpdate();
      statement.close();
    } catch (SQLException exception) {
      Bukkit.getConsoleSender().sendMessage("§cFehler beim Verarbeiten der MySql-Anfrage!");
      exception.printStackTrace();
    }

  }

  public void insert(String... value) {

    try {
      //Build the SQL statement
      StringBuilder statementBuilder = new StringBuilder();
      statementBuilder.append("INSERT INTO stats (uuid, kills, deaths, max_kills) VALUES (");
      for (int i = 0; i < value.length - 1; i++) {
        statementBuilder.append("'");
        statementBuilder.append(value[i]);
        statementBuilder.append("', ");
      }
      statementBuilder.append("'");
      statementBuilder.append(value[value.length - 1]);
      statementBuilder.append("')");

      PreparedStatement statement = connection.prepareStatement(statementBuilder.toString());
      statement.executeUpdate();
      statement.close();
    } catch (SQLException exception) {
      Bukkit.getConsoleSender().sendMessage("§cFehler beim Verarbeiten der MySql-Anfrage!");
      exception.printStackTrace();
    }

  }

  @Override
  public Optional<String> get(String path, Condition condition) {

    try {
      PreparedStatement statement = connection.prepareStatement("SELECT ? FROM stats WHERE ? = ?");
      statement.setString(1, path);
      statement.setString(2, condition.getKey());
      statement.setString(3, condition.getValue());
      ResultSet resultSet = statement.executeQuery();
      try {
        return Optional.ofNullable(resultSet.getString(path));
      } finally {
        resultSet.close();
        statement.close();
      }
    } catch (SQLException exception) {
      Bukkit.getConsoleSender().sendMessage("§cFehler beim Verarbeiten der MySql-Anfrage!");
      exception.printStackTrace();
    }
    return Optional.empty();

  }

}
