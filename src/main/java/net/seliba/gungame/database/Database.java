package net.seliba.gungame.database;

import java.util.Optional;

public interface Database {

  void connect(Credentials credentials);

  void disconnect();

  void set(String key, Object value, Condition condition);

  Optional<String> get(String path, Condition condition);

}
