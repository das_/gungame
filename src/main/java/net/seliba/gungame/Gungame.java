package net.seliba.gungame;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import net.seliba.gungame.commands.GungameCommand;
import net.seliba.gungame.configuration.Config;
import net.seliba.gungame.dao.MapDao;
import net.seliba.gungame.dao.StatsDao;
import net.seliba.gungame.database.Credentials;
import net.seliba.gungame.database.Database;
import net.seliba.gungame.database.MySql;
import net.seliba.gungame.database.Yaml;
import net.seliba.gungame.listener.AsyncPlayerChatListener;
import net.seliba.gungame.listener.BlockEditListener;
import net.seliba.gungame.listener.EntitySpawnListener;
import net.seliba.gungame.listener.FoodLevelChangeListener;
import net.seliba.gungame.listener.InventoryClickListener;
import net.seliba.gungame.listener.InventoryCloseListener;
import net.seliba.gungame.listener.PlayerDamageListener;
import net.seliba.gungame.listener.PlayerDeathListener;
import net.seliba.gungame.listener.PlayerDropListener;
import net.seliba.gungame.listener.PlayerJoinListener;
import net.seliba.gungame.listener.PlayerQuitListener;
import net.seliba.gungame.listener.WeatherChangeListener;
import net.seliba.gungame.maps.MapManager;
import net.seliba.gungame.messages.Messages;
import net.seliba.gungame.stats.Stats;
import net.seliba.gungame.levels.Levels;
import net.seliba.gungame.scoreboard.Scoreboard;
import net.seliba.gungame.database.StorageType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Gungame extends JavaPlugin {

  private Config config;
  private Config mapConfig;
  private Database database;
  private MapDao mapDao;
  private StatsDao statsDao;
  private Scoreboard scoreboard;
  private MapManager mapManager;
  private Stats stats;
  private Levels levels;
  private ScheduledExecutorService executorService;

  @Override
  public void onEnable() {

    //Send message to console
    Bukkit.getConsoleSender().sendMessage("§7[§6Gungame§7] §aErfolgreich gestartet!");

    //Initialize configs and database
    this.initializeConfigs();
    this.initializeDatabase();

    //Initialize the Dao's
    this.mapDao = new MapDao(mapConfig);
    this.statsDao = new StatsDao(database);

    //Initialize the MapManager and load all available Maps
    this.mapManager = new MapManager(this, mapDao, levels, config.getLong("map-voting-time"), config.getLong("time-between-votings"));
    this.mapManager.loadMaps();
    this.mapManager.chooseRandomMap();

    //Initialize the level handler
    this.levels = new Levels(this, "levels.yml", config.getLong("death-level-remove-amount"));

    //Initialize Stats management, the messages and the Scoreboard
    this.stats = new Stats(statsDao);
    this.scoreboard = new Scoreboard(mapManager, stats, ChatColor.translateAlternateColorCodes('&', config.getString("server-name")), this.config.getStringList("scoreboard"));
    new Messages(this);


    //Register Listener and Commands that are needed to run the plugin
    this.registerListener();
    this.registerCommands();

    //Run the location checker and the Scoreboard updater
    this.runLocationChecker();
    this.runScoreboardVotingUpdater();

  }

  @Override
  public void onDisable() {

    //Disconnect from the database
    this.database.disconnect();

    //Shutdown the ExecutorService
    this.executorService.shutdown();

  }

  private void initializeConfigs() {

    //Initialize configs
    this.config = new Config("config.yml", this);
    this.mapConfig = new Config("maps.yml", this);

    //Set default values
    this.config.setDefault("server-name", "ITZYOURGAMES");
    this.config.setDefault("map-voting-time", 30L);
    this.config.setDefault("time-between-votings", 30L);
    this.config.setDefault("time-until-damage-cancellation", 20L);
    this.config.setDefault("heal-on-kill", true);
    this.config.setDefault("cancel-weather-change", true);
    this.config.setDefault("disable-hunger", true);
    this.config.setDefault("disable-item-drop", true);
    this.config.setDefault("teams-allowed", true);
    this.config.setDefault("death-level-remove-amount", 5L);
    this.config.setDefault("scoreboard", this.getDefaultScoreboard());
    this.config.setDefault("storage.type", "YAML");
    this.config.setDefault("storage.hostname", "localhost");
    this.config.setDefault("storage.database", "stats");
    this.config.setDefault("storage.username", "root");
    this.config.setDefault("storage.password", "123");

    //Set default map list
    this.mapConfig.setDefault("map-names", new ArrayList<String>());

    //Save the configs
    this.config.save();
    this.mapConfig.save();

    //Load level file
    this.loadLevelFile();

  }

  private void initializeDatabase() {

    StorageType storageType = null;

    //Try to load the storage type from Config
    try {
      storageType = StorageType.valueOf(config.getString("storage.type").toUpperCase());
    } catch (Exception exception) {
      //The Config or the StorageType is invalid
      Bukkit.getConsoleSender().sendMessage("§7[§6Gungame§7] §cUngültige Speicherart, bitte verwende YAML oder MYSQL!");
      Bukkit.getPluginManager().disablePlugin(this);
    }

    //Initialize the Database
    if(storageType == StorageType.MYSQL) {
      this.database = new MySql();
    } else {
      this.database = new Yaml();
    }

    //Define the Credentials
    Credentials credentials = new Credentials(
        this.config.getString("storage.hostname"),
        this.config.getString("storage.database"),
        this.config.getString("storage.username"),
        this.config.getString("storage.password")
    );

    //Connect to Database
    this.database.connect(credentials);

  }

  private void registerListener() {

    PluginManager pluginManager = Bukkit.getPluginManager();

    //Register Listener
    pluginManager.registerEvents(new AsyncPlayerChatListener(), this);
    pluginManager.registerEvents(new BlockEditListener(mapManager), this);
    pluginManager.registerEvents(new EntitySpawnListener(mapManager), this);
    pluginManager.registerEvents(new FoodLevelChangeListener(mapManager, config.getBoolean("disable-hunger")), this);
    pluginManager.registerEvents(new InventoryClickListener(mapManager), this);
    pluginManager.registerEvents(new InventoryCloseListener(mapManager), this);
    pluginManager.registerEvents(new PlayerDamageListener(mapManager), this);
    pluginManager.registerEvents(new PlayerDeathListener(this, mapManager, levels, stats, config.getLong("time-until-damage-not-kill"), config.getBoolean("heal-on-kill")), this);
    pluginManager.registerEvents(new PlayerDropListener(mapManager, config.getBoolean("disable-item-drop")), this);
    pluginManager.registerEvents(new PlayerJoinListener(mapManager, levels, config.getBoolean("teams-allowed")), this);
    pluginManager.registerEvents(new PlayerQuitListener(mapManager, levels), this);
    pluginManager.registerEvents(new WeatherChangeListener(mapManager, config.getBoolean("cancel-weather-change")), this);

  }

  private void registerCommands() {

    getCommand("gungame").setExecutor(new GungameCommand(mapManager));
    getCommand("gungame").setAliases(Collections.singletonList("gg"));

  }

  private void loadLevelFile() {

    File levelFile = new File(getDataFolder(), "levels.yml");

    if(!levelFile.exists()) {
      try {
        Files.copy(getResource("levels.yml"), Paths.get(levelFile.toURI()), StandardCopyOption.REPLACE_EXISTING);
      } catch (IOException exception) {
        Bukkit.getConsoleSender().sendMessage("§cFehler beim Laden der levels.yml!");
        exception.printStackTrace();
      }
    }

  }

  private void runLocationChecker() {

    //Run every 0.5 seconds
    Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
      //Check if the Player is in the water
      Bukkit.getOnlinePlayers().stream()
          .filter(player -> player.getLocation().getBlockY() < mapManager.getCurrentMap().getMinY())
          .forEach(player -> player.setHealth(0D));
    }, 0L, 10L);

  }

  private void runScoreboardVotingUpdater() {

    //Create the ScheduledExecutorService
    this.executorService = Executors.newSingleThreadScheduledExecutor();

    executorService.scheduleWithFixedDelay(() -> {
      //Update the Scoreboard
      scoreboard.updateScoreboard();

      //Check for voting
      mapManager.getMapVoting().checkForVoting();
    }, 0L, 1L, TimeUnit.SECONDS);

  }

  private List<String> getDefaultScoreboard() {

    return Arrays.asList(
        " ",
        " &fDeine Kills:",
        "&8» &6%KILLS%",
        "  ",
        " &fKillrekord:",
        "&8» &6%MAX_KILLS%",
        "   ",
        " &fAktuelle Map:",
        "&8» &6%MAP%",
        "    ",
        " &fMapwechsel in:",
        "&8» &6%TIME%"
    );

  }

}
