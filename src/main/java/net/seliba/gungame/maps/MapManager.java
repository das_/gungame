package net.seliba.gungame.maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import net.seliba.gungame.dao.MapDao;
import net.seliba.gungame.levels.Levels;
import net.seliba.gungame.messages.MessageType;
import net.seliba.gungame.messages.Messages;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class MapManager {

  private static MapManager instance;
  private MapDao mapDao;
  private MapVoting mapVoting;
  private Levels levels;
  private List<Map> loadedMaps = new ArrayList<>();
  private Map currentMap = null;
  private Random random = new Random();
  private long nextVotingTime;
  private boolean isVotingRunning = false;

  public MapManager(JavaPlugin javaPlugin, MapDao mapDao, Levels levels, long votingTime, long timeBetweenVotings) {

    MapManager.instance = this;
    this.mapDao = mapDao;
    this.mapVoting = new MapVoting(javaPlugin, this, votingTime, timeBetweenVotings);
    this.levels = levels;
    this.nextVotingTime = System.currentTimeMillis() + (timeBetweenVotings * 1000);

  }

  public void loadMaps() {

    this.mapDao.getMapNames().forEach(this::loadMap);

  }

  private void loadMap(String mapName) {

    MapBuilder mapBuilder = new MapBuilder(mapName);
    mapBuilder.setRepresentingItem(mapDao.getRepresentingMaterial(mapName));
    mapBuilder.setSpawnLocation(mapDao.getSpawnLocation(mapName));
    mapBuilder.setMinY(mapDao.getMinY(mapName));
    mapBuilder.setBorderLocations(mapDao.getSpawnBorderLocations(mapName));
    loadedMaps.add(mapBuilder.build());

  }

  public void chooseRandomMap() {

    if(loadedMaps.isEmpty()) {
      Bukkit.getConsoleSender().sendMessage("§cEs wurden noch keine Maps konfiguriert!");
      return;
    }

    //Generate random number
    int randomMap = random.nextInt(loadedMaps.size());

    //Change current map to the new map
    this.currentMap = loadedMaps.get(randomMap);

    //Broadcast message
    Bukkit.broadcastMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.MAP_CHANGED).replaceAll("%MAP%", currentMap.getMapName()));

    //Teleport all Players
    Bukkit.getOnlinePlayers().forEach(player -> player.teleport(currentMap.getSpawnLocation()));

  }

  public void setCurrentMap(String mapName) {

    //Set new map
    this.currentMap = this.getMapByName(mapName).orElse(currentMap);

    //Broadcast message
    Bukkit.broadcastMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.MAP_CHANGED).replaceAll("%MAP%", currentMap.getMapName()));

    //Teleport all Players
    Bukkit.getOnlinePlayers().forEach(player -> player.teleport(currentMap.getSpawnLocation()));

  }

  public Map getCurrentMap() {

    return currentMap;

  }

  public List<Map> getLoadedMaps() {

    return loadedMaps;

  }

  public void respawn(Player player) {

    //Teleport Player and set basic values
    player.teleport(this.currentMap.getSpawnLocation());
    player.setHealth(20D);
    player.setFoodLevel(20);
    player.setGameMode(GameMode.SURVIVAL);

    //Set level
    levels.setLevel(player, 1L);

  }

  public MapVoting getMapVoting() {

    return mapVoting;

  }

  public long getNextVotingTime() {

    return nextVotingTime;

  }

  public void setNextVotingTime(long nextVotingTime) {

    this.nextVotingTime = nextVotingTime;

  }

  public boolean isVotingRunning() {

    return isVotingRunning;

  }

  public void setVotingRunning(boolean votingRunning) {

    isVotingRunning = votingRunning;

  }

  public Optional<Map> getMapByName(String mapName) {

    return loadedMaps.stream()
        .filter(map -> map.getMapName().equals(mapName))
        .findFirst();

  }

  void saveMap(Map map) {

    this.mapDao.saveMap(map);

  }

  public boolean equalsCurrentMap(World world) {

    if(world == null) {
      return false;
    }

    if(this.currentMap == null) {
      return false;
    }

    return this.currentMap.getSpawnLocation().getWorld().getName().equals(world.getName());

  }

  public static MapManager getInstance() {

    return instance;

  }

}
