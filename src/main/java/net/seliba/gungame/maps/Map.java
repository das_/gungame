package net.seliba.gungame.maps;

import org.bukkit.Location;
import org.bukkit.Material;

public class Map {

  private String mapName;
  private Material representingMaterial;
  private Location spawnLocation;
  private Location[] borderLocations;
  private double minY;
  private double minSpawnX;
  private double maxSpawnX;
  private double minSpawnZ;
  private double maxSpawnZ;

  Map(String mapName, Material representingMaterial, Location spawnLocation, double minY, Location location1, Location location2) {

    this.mapName = mapName;
    this.representingMaterial = representingMaterial;
    this.spawnLocation = spawnLocation;
    this.minY = minY;
    this.borderLocations = new Location[] {location1, location2};
    this.minSpawnX = location1.getBlockX() < location2.getBlockX() ? location1.getBlockX() : location2.getBlockX();
    this.maxSpawnX = location1.getBlockX() > location2.getBlockX() ? location1.getBlockX() : location2.getBlockX();
    this.minSpawnZ = location1.getBlockZ() < location2.getBlockZ() ? location1.getBlockZ() : location2.getBlockZ();
    this.maxSpawnZ = location1.getBlockZ() > location2.getBlockZ() ? location1.getBlockZ() : location2.getBlockZ();

  }

  public String getMapName() {

    return mapName;

  }

  public Material getRepresentingMaterial() {

    return representingMaterial;

  }

  public Location getSpawnLocation() {

    return spawnLocation;

  }

  public Location[] getBorderLocations() {

    return borderLocations;

  }

  public double getMinY() {

    return minY;

  }

  public double getMinSpawnX() {

    return minSpawnX;

  }

  public double getMaxSpawnX() {

    return maxSpawnX;

  }

  public double getMinSpawnZ() {

    return minSpawnZ;

  }

  public double getMaxSpawnZ() {

    return maxSpawnZ;

  }

}
