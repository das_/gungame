package net.seliba.gungame.maps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import net.seliba.gungame.event.GungameVoteAddEvent;
import net.seliba.gungame.event.GungameVotingStartEvent;
import net.seliba.gungame.event.GungameVotingStopEvent;
import net.seliba.gungame.messages.MessageType;
import net.seliba.gungame.messages.Messages;
import net.seliba.gungame.utils.ItemStackBuilder;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

public class MapVoting {

  private JavaPlugin javaPlugin;
  private MapManager mapManager;
  private long votingTime;
  private long timeBetweenVotings;
  private java.util.Map<Player, Map> playerVotings = new HashMap<>();
  private java.util.Map<Map, Long> votingAmounts = new HashMap<>();
  private Random random = new Random();

  MapVoting(JavaPlugin javaPlugin, MapManager mapManager, long votingTime, long timeBetweenVotings) {

    this.javaPlugin = javaPlugin;
    this.mapManager = mapManager;
    this.votingTime = votingTime;
    this.timeBetweenVotings = timeBetweenVotings;

  }

  private void startVoting() {

    //Change vote running state
    this.mapManager.setVotingRunning(true);

    //Add a Scheduler for vote ending
    Bukkit.getScheduler().runTaskLaterAsynchronously(javaPlugin, this::stopVoting, votingTime * 20);

    //Set votes of all maps to 0
    this.mapManager.getLoadedMaps().forEach(map -> votingAmounts.put(map, 0L));

    //Start voting for all Players
    Bukkit.getOnlinePlayers().forEach(this::startVoting);

    //Trigger the GungameVotingStartEvent
    Bukkit.getPluginManager().callEvent(new GungameVotingStartEvent(this.mapManager.getLoadedMaps(), this.votingTime));

  }

  public void startVoting(Player player) {

    //Send message
    player.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.VOTING_STARTED));

    //Open voting Inventory
    this.reloadVotingInventory();

  }

  private void reloadVotingInventory() {

    Inventory inventory = Bukkit.createInventory(null, mapManager.getLoadedMaps().size() / 9, "§aMap-Voting");

    for(Map map : mapManager.getLoadedMaps()) {
      inventory.addItem(new ItemStackBuilder(
          map.getRepresentingMaterial())
            .name("§6" + map.getMapName())
            .lore(Messages.get(MessageType.VOTING_LORE).replaceAll("%NAME%", map.getMapName()).replaceAll("%AMOUNT%", String.valueOf(votingAmounts.get(map))))
            .lore("§6" + votingAmounts.get(map) + " Votes")
            .build()
      );
    }

    Bukkit.getOnlinePlayers().forEach(player -> player.openInventory(inventory));

  }

  public void stopVoting() {

    //Set voting running to false and set new voting time
    this.mapManager.setNextVotingTime(System.currentTimeMillis() + (timeBetweenVotings * 60 * 1000));
    this.mapManager.setVotingRunning(false);

    //Get most voted map and the world of the map
    Map oldMap = mapManager.getCurrentMap();
    Map newMap = this.getMostVotedMap();
    World newMapWorld = newMap.getSpawnLocation().getWorld();

    //Set the new map
    this.mapManager.setCurrentMap(newMap.getMapName());

    //Trigger the GungameVotingStopEvent
    Bukkit.getPluginManager().callEvent(new GungameVotingStopEvent(newMap, oldMap, playerVotings, votingAmounts));

    //Set defaults of new world
    newMapWorld.setTime(1000L);
    newMapWorld.setThundering(false);
    newMapWorld.setStorm(false);
    newMapWorld.setGameRuleValue("doDaylightCycle", "false");
    newMapWorld.setGameRuleValue("doWeatherCycle", "false");
    newMapWorld.getEntities().stream()
        .filter(entity -> entity.getType() != EntityType.PLAYER && entity.getType() != EntityType.ARMOR_STAND)
        .forEach(Entity::remove);

    //Reset voting results
    this.mapManager.getLoadedMaps().forEach(map -> votingAmounts.put(map, 0L));
    this.playerVotings.clear();

  }

  private Map getMostVotedMap() {

    List<Map> mostVotedMaps = new ArrayList<>();

    //Iterate over all maps and their votes
    for(Entry<Map, Long> mapLongEntry : votingAmounts.entrySet()) {
      Map currentLoopMap = mapLongEntry.getKey();
      Long mostVotedMapVotes = votingAmounts.get(mostVotedMaps.get(0) == null ? mapLongEntry.getValue() : mostVotedMaps.get(0));

      //Check if this is the first loop
      if(mostVotedMaps.isEmpty()) {
        mostVotedMaps.add(currentLoopMap);
      }

      //Compare the votes
      if(mostVotedMapVotes > mapLongEntry.getValue()) {
        //Map is not one of the most voted maps
        continue;
      } else if(mostVotedMapVotes.equals(mapLongEntry.getValue())) {
        //Map is one of the most voted maps
        mostVotedMaps.add(currentLoopMap);
      } else {
        //Map is new most voted map
        mostVotedMaps.clear();
        mostVotedMaps.add(currentLoopMap);
      }
    }

    //Get random most voted map
    return mostVotedMaps.get(random.nextInt(mostVotedMaps.size()));

  }

  public void checkForVoting() {

    //Check if there should be a voting
    if(mapManager.getNextVotingTime() >= System.currentTimeMillis()) {

      //Check for voting announcement
      long timeUntilNextVoting = System.currentTimeMillis() - mapManager.getNextVotingTime();
      String timeString = "";
      if(timeUntilNextVoting < 1800500 && timeUntilNextVoting > 1799500) {
        //30 minutes remaining
        timeString = "30 Minuten";
      } else if(timeUntilNextVoting < 900500 && timeUntilNextVoting > 899500) {
        //15 minutes remaining
        timeString = "15 Minuten";
      } else if(timeUntilNextVoting < 60500 && timeUntilNextVoting > 59500) {
        //1 minute remaining
        timeString = "1 Minute";
      } else if(timeUntilNextVoting < 30500 && timeUntilNextVoting > 29500) {
        //30 seconds remaining
        timeString = "30 Sekunden";
      } else {
        return;
      }

      Bukkit.broadcastMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.NEXT_VOTING).replaceAll("%TIME%", timeString));

      return;
    }

    //Check if there is currently a voting running
    if(mapManager.isVotingRunning()) {
      return;
    }

    //Start a new map voting
    this.startVoting();

  }

  public void addVote(Player player, Map map) {

    //Check if the Player has already voted
    if(this.hasVoted(player)) {
      //Remove the old vote
      this.removeVote(player);
    }

    //Add the new vote
    this.playerVotings.put(player, map);
    this.votingAmounts.put(map, votingAmounts.get(map) + 1);

    //Trigger the GungameVoteAddEvent
    Bukkit.getPluginManager().callEvent(new GungameVoteAddEvent(player, map));

    //Reload voting Inventory
    this.reloadVotingInventory();

  }

  public void removeVote(Player player) {

    this.votingAmounts.put(playerVotings.get(player), votingAmounts.get(playerVotings.get(player)) - 1);
    this.playerVotings.remove(player);

    //Reload voting Inventory
    this.reloadVotingInventory();

  }

  public boolean hasVoted(Player player) {

    return playerVotings.containsKey(player);

  }

}
