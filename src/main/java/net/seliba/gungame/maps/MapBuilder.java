package net.seliba.gungame.maps;

import org.bukkit.Location;
import org.bukkit.Material;

class MapBuilder {

  private String name;
  private Material representingMaterial;
  private Location spawnLocation;
  private double minY;
  private Location[] locations;

  MapBuilder(String name) {

    this.name = name;

  }

  void setRepresentingItem(Material material) {

    this.representingMaterial = material;

  }

  void setSpawnLocation(Location spawnLocation) {

    this.spawnLocation = spawnLocation;

  }

  void setMinY(double minY) {

    this.minY = minY;

  }

  void setBorderLocations(Location[] locations) {

    this.locations = locations;

  }

  Map build() {

    return new Map(name, representingMaterial, spawnLocation, minY, locations[0], locations[1]);

  }

}
