package net.seliba.gungame.maps;

import java.util.HashMap;
import java.util.Map;
import net.seliba.gungame.messages.MessageType;
import net.seliba.gungame.messages.Messages;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class MapCreator {

  private static Map<Player, MapCreator> savedCreators = new HashMap<>();

  private Player player;
  private MapBuilder playerBuilder;
  private SetupState nextSetupState;
  private Location[] spawnBorderLocations = new Location[2];
  private boolean dataEntering = false;

  private MapCreator(Player player) {

    MapCreator.savedCreators.put(player, this);

    this.player = player;
    this.nextSetupState = SetupState.NAME;

  }

  public void nextStep() {

    //Check if the Player is currently in the data entering mode
    if(this.dataEntering) {
      this.fetchData();
      return;
    }

    switch (nextSetupState) {
      case NAME:
        player.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.SETUP_NAME));
        break;
      case MATERIAL:
        player.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.SETUP_MATERIAL));
        this.dataEntering = true;
        break;
      case SPAWN:
        player.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.SETUP_SPAWN_LOCATION));
        this.dataEntering = true;
        break;
      case BORDER_LOCATION_1:
        player.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.SETUP_BORDER_LOCATION_1));
        this.dataEntering = true;
        break;
      case BORDER_LOCATION_2:
        player.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.SETUP_BORDER_LOCATION_2));
        this.dataEntering = true;
        break;
      case MIN_Y:
        player.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.SETUP_MIN_Y));
        this.dataEntering = true;
        break;
      case FINISHED:
        player.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.SETUP_FINISH_CONFIRMATION));
        this.dataEntering = true;
        break;
      default:
        //Should not be executed
        break;
    }

  }

  private void fetchData() {

    switch (nextSetupState) {
      case MATERIAL:
        //Define the ItemStack in the Player's hand
        ItemStack itemStack = this.player.getInventory().getItemInHand();

        //Check if the Player is holding an ItemStack in his main hand
        if(itemStack.getType() == Material.AIR) {
          player.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.NO_ITEM));
          return;
        }

        this.setMaterial(itemStack.getType());
        break;
      case SPAWN:
        this.setSpawnLocation(this.player.getLocation());
        break;
      case BORDER_LOCATION_1:
        this.setBorderLocation1(this.player.getLocation());
        break;
      case BORDER_LOCATION_2:
        this.setBorderLocation2(this.player.getLocation());
        break;
      case MIN_Y:
        this.setMinY(this.player.getLocation().getBlockY());
        break;
      case FINISHED:
        MapManager.getInstance().saveMap(this.playerBuilder.build());
        savedCreators.remove(player, this);
        player.sendMessage(Messages.get(MessageType.PREFIX) + Messages.get(MessageType.SETUP_FINSHED));
        break;
      default:
        //Should not be executed
        break;
    }

  }

  public void setName(String name) {

    //Check if the Setup is currently in the right state
    if(this.nextSetupState != SetupState.NAME) {
      return;
    }

    this.playerBuilder = new MapBuilder(name);

    //Proceed to the next step
    this.dataEntering = false;
    this.nextSetupState = SetupState.MATERIAL;
    this.nextStep();

  }

  private void setMaterial(Material material) {

    //Check if the Setup is currently in the right state
    if(this.nextSetupState != SetupState.MATERIAL) {
      return;
    }

    this.playerBuilder.setRepresentingItem(material);

    //Proceed to the next step
    this.dataEntering = false;
    this.nextSetupState = SetupState.SPAWN;
    this.nextStep();

  }

  private void setSpawnLocation(Location spawnLocation) {

    //Check if the Setup is currently in the right state
    if(this.nextSetupState != SetupState.SPAWN) {
      return;
    }

    this.playerBuilder.setSpawnLocation(spawnLocation);

    //Proceed to the next step
    this.dataEntering = false;
    this.nextSetupState = SetupState.BORDER_LOCATION_1;
    this.nextStep();

  }

  private void setBorderLocation1(Location location_1) {

    //Check if the Setup is currently in the right state
    if(this.nextSetupState != SetupState.BORDER_LOCATION_1) {
      return;
    }

    this.spawnBorderLocations[0] = location_1;

    //Proceed to the next step
    this.dataEntering = false;
    this.nextSetupState = SetupState.BORDER_LOCATION_2;
    this.nextStep();

  }

  private void setBorderLocation2(Location location_2) {

    //Check if the Setup is currently in the right state
    if(this.nextSetupState != SetupState.BORDER_LOCATION_2) {
      return;
    }

    this.spawnBorderLocations[1] = location_2;
    this.playerBuilder.setBorderLocations(this.spawnBorderLocations);

    //Proceed to the next step
    this.dataEntering = false;
    this.nextSetupState = SetupState.MIN_Y;
    this.nextStep();

  }

  private void setMinY(double y) {

    //Check if the Setup is currently in the right state
    if(this.nextSetupState != SetupState.MIN_Y) {
      return;
    }

    this.playerBuilder.setMinY(y);

    //Proceed to the next step
    this.dataEntering = false;
    this.nextSetupState = SetupState.FINISHED;
    this.nextStep();

  }

  public SetupState getNextSetupState() {

    return nextSetupState;

  }

  public static MapCreator get(Player player) {

    //Check if the Player has a MapCreator instance
    if(!savedCreators.containsKey(player)) {
      return new MapCreator(player);
    }

    //Access the MapCreator instance of the Player
    return savedCreators.get(player);

  }

  public static boolean isInSetup(Player onlinePlayer) {

    return savedCreators.containsKey(onlinePlayer);

  }

}
