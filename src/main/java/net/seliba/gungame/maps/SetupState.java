package net.seliba.gungame.maps;

public enum SetupState {

  NAME(),
  MATERIAL(),
  SPAWN(),
  MIN_Y(),
  BORDER_LOCATION_1(),
  BORDER_LOCATION_2(),
  FINISHED()

}
