package net.seliba.gungame.dao;

import java.util.Optional;
import net.seliba.gungame.database.Condition;
import net.seliba.gungame.database.Database;
import net.seliba.gungame.database.MySql;
import org.bukkit.entity.Player;

public class StatsDao {

  private Database database;

  public StatsDao(Database database) {

    this.database = database;
  }

  private void setDefaultStats(Player player) {

    Optional optional = database.get("kills", new Condition("uuid", player.getUniqueId().toString()));

    if(!optional.isPresent()) {
      if(database instanceof MySql) {
        MySql mySql = (MySql) database;
        mySql.insert(player.getUniqueId().toString(), "0", "0", "0");
      }
    }

  }

  public void addDeath(Player player) {

    this.setDefaultStats(player);

    database.set(
        "deaths",
        String.valueOf(getKills(player) + 1),
        new Condition("uuid", player.getUniqueId().toString())
    );

  }

  public void addKill(Player player) {

    this.setDefaultStats(player);

    database.set(
        "kills",
        String.valueOf(getKills(player) + 1),
        new Condition("uuid", player.getUniqueId().toString())
    );

  }

  public Long getKills(Player player) {

    Optional optional = database.get("kills", new Condition("uuid", player.getUniqueId().toString()));

    if (!optional.isPresent()) {
      return 0L;
    }

    return Long.valueOf((String) optional.get());

  }

  public Long getDeaths(Player player) {

    Optional optional = database.get("deaths", new Condition("uuid", player.getUniqueId().toString()));

    if (!optional.isPresent()) {
      return 0L;
    }

    return Long.valueOf((String) optional.get());

  }

  public Long getMaxKills(Player player) {

    Optional optional = database.get("max_kills", new Condition("uuid", player.getUniqueId().toString()));

    if (!optional.isPresent()) {
      return 0L;
    }

    return Long.valueOf((String) optional.get());

  }

  public void addMaxKills(Player player) {

    this.setDefaultStats(player);

    database.set(
        "max_kills",
        String.valueOf(getKills(player) + 1),
        new Condition("uuid", player.getUniqueId().toString())
    );

  }

}
