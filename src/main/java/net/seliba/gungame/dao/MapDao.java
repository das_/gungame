package net.seliba.gungame.dao;

import java.util.List;
import net.seliba.gungame.configuration.Config;
import net.seliba.gungame.maps.Map;
import org.bukkit.Location;
import org.bukkit.Material;

public class MapDao {

  private Config mapConfig;

  public MapDao(Config mapConfig) {

    this.mapConfig = mapConfig;

  }

  public List<String> getMapNames() {

    return mapConfig.getStringList("map-names");

  }

  public Material getRepresentingMaterial(String mapName) {

    return Material.valueOf(mapConfig.getString(mapName + ".material"));

  }

  public Location getSpawnLocation(String mapName) {

    return mapConfig.getLocation(mapName + ".spawn");

  }

  public Location[] getSpawnBorderLocations(String mapName) {

    Location[] borderLocations = new Location[2];

    borderLocations[0] = mapConfig.getLocation(mapName + ".border-1");
    borderLocations[1] = mapConfig.getLocation(mapName + ".border-2");

    return borderLocations;

  }

  public Double getMinY(String mapName) {

    return mapConfig.getDouble(mapName + ".minY");

  }

  public void saveMap(Map map) {

    List<String> mapNames = this.getMapNames();
    mapNames.add(map.getMapName());

    this.mapConfig.set("map-names", mapNames);
    this.mapConfig.set(map.getMapName() + ".material", map.getRepresentingMaterial().name());
    this.mapConfig.setLocation(map.getMapName() + ".spawn", map.getSpawnLocation());
    this.mapConfig.setLocation(map.getMapName() + ".border-1", map.getBorderLocations()[0]);
    this.mapConfig.setLocation(map.getMapName() + ".border-2", map.getBorderLocations()[1]);
    this.mapConfig.set(map.getMapName() + ".minY", map.getMinY());
    this.mapConfig.save();

  }

}
