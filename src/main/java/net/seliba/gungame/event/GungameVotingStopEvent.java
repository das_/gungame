package net.seliba.gungame.event;

import net.seliba.gungame.maps.Map;
import net.seliba.gungame.maps.MapManager;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GungameVotingStopEvent extends Event {

  private static final HandlerList HANDLERS = new HandlerList();

  private Map newMap;
  private Map oldMap;
  private java.util.Map<Player, Map> playerVotes;
  private java.util.Map<Map, Long> mapVotes;

  public GungameVotingStopEvent(Map newMap, Map oldMap, java.util.Map<Player, Map> playerVotes, java.util.Map<Map, Long> mapVotes) {

    this.newMap = newMap;
    this.oldMap = oldMap;
    this.playerVotes = playerVotes;
    this.mapVotes = mapVotes;

  }

  public Map getNewMap() {

    return newMap;

  }

  public Map getOldMap() {

    return oldMap;

  }

  public java.util.Map<Player, Map> getPlayerVotes() {

    return playerVotes;

  }

  public java.util.Map<Map, Long> getMapVotes() {

    return mapVotes;

  }

  public void setCancelled(boolean cancelled) {

    if(cancelled) {
      MapManager.getInstance().setCurrentMap(oldMap.getMapName());
    }

  }

  @Override
  public HandlerList getHandlers() {

    return HANDLERS;

  }

  public static HandlerList getHandlerList() {

    return HANDLERS;

  }

}
