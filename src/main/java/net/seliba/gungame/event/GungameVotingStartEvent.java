package net.seliba.gungame.event;

import java.util.List;
import net.seliba.gungame.maps.Map;
import net.seliba.gungame.maps.MapManager;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GungameVotingStartEvent extends Event {

  private static final HandlerList HANDLERS = new HandlerList();

  private List<Map> mapList;
  private long votingTimeInSeconds;

  public GungameVotingStartEvent(List<Map> mapList, long votingTimeInSeconds) {

    this.mapList = mapList;
    this.votingTimeInSeconds = votingTimeInSeconds;

  }

  public List<Map> getMaps() {

    return mapList;

  }

  public long getVotingTimeInSeconds() {

    return votingTimeInSeconds;

  }

  @Deprecated
  public void setCancelled(boolean cancelled) {

    if(cancelled) {
      MapManager.getInstance().getMapVoting().stopVoting();
    }

  }

  @Override
  public HandlerList getHandlers() {

    return HANDLERS;

  }

  public static HandlerList getHandlerList() {

    return HANDLERS;

  }

}
