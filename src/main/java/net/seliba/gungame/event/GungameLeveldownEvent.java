package net.seliba.gungame.event;

import net.seliba.gungame.levels.Levels;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class GungameLeveldownEvent extends Event {

  private static final HandlerList HANDLERS = new HandlerList();

  private Player player;
  private Player killer;
  private long oldLevel;
  private long newLevel;
  private ItemStack[] equipment;

  public GungameLeveldownEvent(Player player, Player killer, long oldLevel, long newLevel, ItemStack[] equipment) {

    this.player = player;
    this.killer = killer;
    this.oldLevel = oldLevel;
    this.newLevel = newLevel;
    this.equipment = equipment;

  }

  public Player getPlayer() {

    return player;

  }

  public Player getKiller() {

    return killer;

  }

  public long getOldLevel() {

    return oldLevel;

  }

  public long getNewLevel() {

    return newLevel;

  }

  public ItemStack[] getEquipment() {

    return equipment;

  }

  public void setCancelled(boolean cancelled) {

    if(cancelled) {
      Levels.getInstance().setLevel(player, oldLevel);
    }

  }

  @Override
  public HandlerList getHandlers() {

    return HANDLERS;

  }

  public static HandlerList getHandlerList() {

    return HANDLERS;

  }

}
