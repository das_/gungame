package net.seliba.gungame.event;

import net.seliba.gungame.maps.Map;
import net.seliba.gungame.maps.MapManager;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GungameVoteAddEvent extends Event {

  private static final HandlerList HANDLERS = new HandlerList();

  private Player player;
  private Map map;

  public GungameVoteAddEvent(Player player, Map map) {

    this.player = player;
    this.map = map;

  }

  public Player getPlayer() {

    return player;

  }

  public Map getMap() {

    return map;

  }

  public void setCancelled(boolean cancelled) {

    if(cancelled) {
      MapManager.getInstance().getMapVoting().removeVote(player);
    }

  }

  @Override
  public HandlerList getHandlers() {

    return HANDLERS;

  }

  public static HandlerList getHandlerList() {

    return HANDLERS;

  }

}
