# Gungame 

A simple, highly configurable and easy to use Gungame plugin for Spigot servers written in Java.

It currently supports the MySQL database and the YAML file format as data storage. More databases will be coming soon.

©2019 Seliba